package main

import (
  "os"
  "os/exec"
  "syscall"
  "fmt"
)

func main() {
  switch os.Args[1] {
  case "run":
    run()
  case "child":
    child()
  default:
    panic("Unknown command")
  }
}

func run() {
  // fork a _clone_ process off in the command (execute the self process)
  cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
  cmd.Stdin = os.Stdin
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  cmd.SysProcAttr = &syscall.SysProcAttr{
    Cloneflags:  syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID,
  }

  must(cmd.Run())
}

func child() {
  fmt.Printf("running %v as %v\n", os.Args[2:], os.Getpid())

  cmd := exec.Command(os.Args[2], os.Args[3:]...)
  cmd.Stdin = os.Stdin
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr

  must(cmd.Run())
}

func must(err error) {
  if err != nil {
    panic(err)
  }
}
