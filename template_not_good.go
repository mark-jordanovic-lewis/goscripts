//Users/_morbid.slug/.local/bin/go_run "$0" "$@"; exit "$?"

// this will only work with additional script in your path that has `go run $0` in.
// - location above in the shebang simulator
package main

import (
  "os"
  "fmt"
)

func main() {
    s := "World!"
    if len(os.Args) > 1 {
      s = os.Args[1]
    }
    fmt.Printf("Hello, %v\n", s)
    if s == "fail" {
      os.Exit(30)
    }
}
