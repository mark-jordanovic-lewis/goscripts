FROM debian:stable-backports

RUN apt-get update && apt-get -y install git golang sudo
RUN mkdir /go
ENV GOPATH /go
RUN go get github.com/erning/gorun
RUN ln -s $GOPATH/bin/gorun /usr/local/bin/gorun
RUN useradd --create-home --password h3lp7ulpwd _morbid.slug
RUN adduser _morbid.slug sudo

COPY --chown=_morbid.slug:_morbid.slug * /Users/_morbid.slug/
WORKDIR /Users/_morbid.slug
CMD su _morbid.slug -c /bin/bash
