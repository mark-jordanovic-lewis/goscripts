#!/usr/local/bin/gorun
// this will only work as a script from the console - mac friendly version
// have to run:
//   `go get github.com/erning/gorun`
//   `ln -s $GOPATH/gorun/gorun /usr/bin/gorun`

package main

import (
  "os"
  "fmt"
)

func main() {
    s := "World!"
    if len(os.Args) > 1 {
      s = os.Args[1]
    }
    fmt.Printf("Hello, %v\n", s)
    if s == "fail" {
      os.Exit(30)
    }
}
